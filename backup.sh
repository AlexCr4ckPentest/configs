#!/usr/bin/env sh

CFG=~/.config
HOSTNAME=$(uname -n)
BACKUP_DIR=$(pwd)/$HOSTNAME

if [[ ! -d $BACKUP_DIR ]]; then mkdir $BACKUP_DIR; fi

echo "Host: $HOSTNAME"
echo "Config dir: $CFG"
echo "Backup dir: $BACKUP_DIR"

cp -r $CFG/i3 $BACKUP_DIR/.
cp -r $CFG/polybar $BACKUP_DIR/.
cp -r $CFG/alacritty $BACKUP_DIR/.
cp -r $CFG/dunst $BACKUP_DIR/.

cp ~/.vimrc vim/vimrc
cp ~/.tmux.conf tmux/tmux.conf
cp $CFG/picom/picom.conf picom/picom-$HOSTNAME.conf
cp $CFG/clipster/clipster.ini clipster/.

