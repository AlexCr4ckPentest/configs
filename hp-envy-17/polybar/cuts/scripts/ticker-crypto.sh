#!/bin/sh

API="https://api.kraken.com/0/public/Ticker"

btc_usd=$(curl -sf $API?pair=BTCUSD | jq -r ".result.XXBTZUSD.c[0]")
xmr_usd=$(curl -sf $API?pair=XMRUSD | jq -r ".result.XXMRZUSD.c[0]")

btc_usd=$(printf "%.2f" "$btc_usd")
xmr_usd=$(printf "%.2f" "$xmr_usd")

echo "BTC: \$$btc_usd | XMR: \$$xmr_usd"

