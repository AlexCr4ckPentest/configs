#!/usr/bin/env sh

CFG=~/.config
HOSTNAME=$(uname -n)
BACKUP_DIR=$(pwd)/$HOSTNAME

if [[ ! -d $BACKUP_DIR ]]; then
	echo "No configs for: $HOSTNAME";
	exit 1;
fi

echo "Host: $HOSTNAME"
echo "Backup dir: $BACKUP_DIR"
echo "Extract dir: $CFG"

cp -r $BACKUP_DIR/* $CFG/.

cp vim/vimrc ~/.vimrc
cp tmux/tmux.conf ~/.tmux.conf
cp picom/picom-$HOSTNAME.conf $CFG/picom.conf
cp -r clipster $CFG/.
cp dunst/dunst-$HOSTNAME.conf $CFG/dunst.conf

#curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
#    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim &> /dev/null

