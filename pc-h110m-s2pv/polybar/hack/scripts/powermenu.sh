#!/usr/bin/env bash

## Author  : Aditya Shakya
## Mail    : adi1090x@gmail.com
## Github  : @adi1090x
## Twitter : @adi1090x

## Edited by : AlexCr4ckPentest
## Github    : @AlexCr4ckPentest

dir="~/.config/polybar/hack/scripts/rofi"
uptime=$(uptime -p | sed -e 's/up //g')
rofi_command="rofi -theme $dir/powermenu.rasi"

# Options
shutdown=" Shutdown"
reboot=" Restart"
lock=" Lock"
suspend=" Sleep"
logout=" Logout"

confirm_choise() {
	rofi -dmenu -i -no-fixed-num-lines -p "Are You Sure?: " \
		-theme $dir/confirm.rasi
}

msg() {
	rofi -theme "$dir/message.rasi" -e "Available Options: [Y]es / [N]o"
}

options="$lock\n$suspend\n$logout\n$reboot\n$shutdown"
chosen="$(echo -e "$options" | $rofi_command -p "Uptime: $uptime" -dmenu -selected-row 0)"

#case $chosen in
#    $shutdown)
#	ans=$(confirm_choise &)
#	if [ "${ans,,}" == "y" ]; then loginctl poweroff
#	elif [ "${ans,,}" == "n" ]; then exit 0
#        else msg
#        fi
#        ;;
#    $reboot)
#	ans=$(confirm_choise &)
#	if [ "${ans,,}" == "y" ]; then loginctl reboot
#	elif [ "${ans,,}" == "n" ]; then exit 0
#        else msg
#        fi
#        ;;
#    $lock) loginctl lock-session ;;
#    $suspend)
#	ans=$(confirm_choise &)
#	if [ "${ans,,}" == "y" ]; then
#		#mpc -q pause
#		#amixer set Master mute
#		loginctl suspend
#	elif [ "${ans,,}" == "n" ]; then exit 0
#        else msg
#        fi
#        ;;
#    $logout)
#	ans=$(confirm_choise &)
#	if [ "${ans,,}" == "y" ]; then
#		case "$DESKTOP_SESSION" in
#			"Openbox") openbox --exit ;;
#			"bspwn") bspc quit ;;
#			"i3") i3-msg exit ;;
#		esac
#	elif [ "${ans,,}" == "n" ]; then exit 0
#        else msg
#        fi
#	;;
#esac

case $chosen in
    $shutdown) loginctl poweroff ;;
    $reboot) loginctl reboot ;;
    $lock) loginctl lock-session ;;
    $suspend)
		#mpc -q pause
		#amixer set Master mute
		loginctl suspend
        ;;
    $logout)
		case "$DESKTOP_SESSION" in
			"Openbox") openbox --exit ;;
			"bspwn") bspc quit ;;
			"i3") i3-msg exit ;;
		esac
	    ;;
esac
